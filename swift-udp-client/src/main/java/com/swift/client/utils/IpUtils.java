package com.swift.client.utils;

import com.alibaba.fastjson.JSONObject;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

/**
 * @author zl
 * @version 1.0
 * @date 2021/9/28 10:38
 */
public class IpUtils {
    private RestTemplate restTemplate;

    public IpUtils(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public String getIpv4(String url) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity entity = new HttpEntity<>(headers);
        ResponseEntity<String> result = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
        String b = result.getBody();
        JSONObject jsonObject = JSONObject.parseObject(b);
        String ip = jsonObject.getString("ip");
        return ip;
    }

}
