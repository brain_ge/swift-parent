package com.swift.client.web;

import com.swift.client.udp.UdpClient1;
import com.swift.client.udp.UdpClient3;
import io.netty.buffer.Unpooled;
import io.netty.channel.socket.DatagramPacket;
import io.netty.util.CharsetUtil;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.net.InetSocketAddress;

/**
 * @author zl
 * @version 1.0
 * @date 2021/9/28 11:13
 */
@RestController
@RequestMapping("/")
public class TestController {
    @Autowired
    private UdpClient3 udpClient3;

    @GetMapping("/sendMessage")
    public String sendMessage(@ApiParam("ip") @RequestParam("ip") String ip,
                              @ApiParam("port") @RequestParam("port") Integer port,
                              @ApiParam("message") @RequestParam("message") String message){
        try {
            udpClient3.sendMessage(ip,port,message);
            return "发送成功";
        } catch (InterruptedException e) {
            e.printStackTrace();
            return "发送失败";
        }
    }


}
