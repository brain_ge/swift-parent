package com.swift.client.netty;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class ClientServer {
    @Autowired
    private ImHandler imHandler;
    @Value("${serverUrl}")
    private String serverUrl;
    @Value("${serverPort}")
    private Integer serverPort;

    @PostConstruct
    public void startClient() {
        //ImSocketClient imSocketClient = new ImSocketClient(serverUrl, serverPort, imHandler);
       // imSocketClient.startClient();
    }

}
