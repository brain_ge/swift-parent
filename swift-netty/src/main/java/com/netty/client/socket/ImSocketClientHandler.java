package com.netty.client.socket;

import com.netty.model.SentBody;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;

public interface ImSocketClientHandler {

    /**
     * 链接成功之后调用该方法
     */
    void channelActive(ChannelHandlerContext ctx);

    /**
     * 收到消息之后调用该方法
     */
    void channelRead0(Channel channel, SentBody sentBody);

    /**
     * 断开链接
     */
    void channelUnregistered(ChannelHandlerContext ctx);


}
