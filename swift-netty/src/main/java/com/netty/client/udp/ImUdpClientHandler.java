package com.netty.client.udp;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.socket.DatagramPacket;

public interface ImUdpClientHandler {

    /**
     * 链接成功之后调用该方法
     */
    void channelActive(ChannelHandlerContext ctx);

    /**
     * 收到消息之后调用该方法
     */
    void channelRead0(Channel channel, DatagramPacket datagramPacket);



}
