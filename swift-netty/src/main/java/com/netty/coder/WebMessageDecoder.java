package com.netty.coder;

import com.google.protobuf.MessageLite;
import com.google.protobuf.MessageLiteOrBuilder;
import com.netty.constant.Constants;
import com.netty.model.Pong;
import com.netty.model.SentBody;
import com.netty.model.proto.SentBodyProto;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufInputStream;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;
import io.netty.handler.codec.http.websocketx.BinaryWebSocketFrame;

import java.io.InputStream;
import java.util.List;

import static io.netty.buffer.Unpooled.wrappedBuffer;


public class WebMessageDecoder extends MessageToMessageDecoder<BinaryWebSocketFrame> {

    @Override
    protected void decode(ChannelHandlerContext context, BinaryWebSocketFrame frame, List<Object> list) throws Exception {

        ByteBuf buffer = frame.content();

        InputStream inputStream = new ByteBufInputStream(buffer);

        SentBodyProto.Model bodyProto = SentBodyProto.Model.parseFrom(inputStream);

        list.add(bodyProto);
    }
}
