package com.netty.disruptor;

import com.netty.model.SentBody;
import io.netty.channel.Channel;

/**
 * @author zl
 * @version 1.0
 * @date 2021/10/8 13:42
 */
public class TranslatorDataWrapper {

    private SentBody sentBody;

    private Channel channel;

    public SentBody getSentBody() {
        return sentBody;
    }

    public void setSentBody(SentBody sentBody) {
        this.sentBody = sentBody;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }
}
