package com.netty.disruptor;

import com.lmax.disruptor.EventHandler;
import com.lmax.disruptor.WorkHandler;
import io.netty.channel.Channel;

/**
 * @author zl
 * @version 1.0
 * @date 2021/10/8 13:49
 */
public class DisruptorConsumer implements EventHandler<TranslatorDataWrapper> ,WorkHandler<TranslatorDataWrapper> {


    @Override
    public void onEvent(TranslatorDataWrapper translatorDataWrapper, long l, boolean b) throws Exception {
        onEvent(translatorDataWrapper);
    }

    @Override
    public void onEvent(TranslatorDataWrapper translatorDataWrapper) throws Exception {

    }

}
