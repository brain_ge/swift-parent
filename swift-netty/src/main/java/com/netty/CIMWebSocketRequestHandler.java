package com.netty;

/**
 * 请求处理接口,所有的请求实现必须实现此接口
 */

import com.netty.model.proto.SentBodyProto;
import io.netty.channel.Channel;

public interface CIMWebSocketRequestHandler {

    /**
     * 链接成功之后调用该方法
     */
    void channelActive(Channel channel);

    /**
     * 收到消息之后调用该方法
     */
    void channelRead0(Channel channel, SentBodyProto.Model sentBody);

    /**
     * 断开链接
     */
    void channelUnregistered(Channel channel);
}
