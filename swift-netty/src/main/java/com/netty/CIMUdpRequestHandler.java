package com.netty;

/**
 * 请求处理接口,所有的请求实现必须实现此接口
 */
import io.netty.channel.Channel;
import io.netty.channel.socket.DatagramPacket;


public interface CIMUdpRequestHandler {

    /**
     * 链接成功之后调用该方法
     */
    void channelActive(Channel channel);

    /**
     * 收到消息之后调用该方法
     */
    void channelRead0(Channel channel, DatagramPacket object);


}
