package com.netty;

/**
 * 请求处理接口,所有的请求实现必须实现此接口
 */

import io.netty.channel.Channel;

public interface CIMSocketRequestHandler {

    /**
     * 链接成功之后调用该方法
     */
    void channelActive(Channel channel);

    /**
     * 处理收到客户端从长链接发送的数据
     */
    void channelRead0(Channel channel, Object body);

    /**
     * 断开链接
     */
    void channelUnregistered(Channel channel);

}
