/**
 * **************************************************************************************
 *
 * @Author 1044053532@qq.com
 * @License http://www.apache.org/licenses/LICENSE-2.0
 * **************************************************************************************
 */
package com.netty.constant;

import io.netty.util.AttributeKey;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Constants {

    public static ExecutorService executorService = Executors.newFixedThreadPool(5);


    public interface ImserverConfig {
        //连接空闲时间
        int READ_IDLE_TIME = 60;//秒
        //发送心跳包循环时间
        int WRITE_IDLE_TIME = 10;//秒
        //心跳响应 超时时间
        int PING_TIME_OUT = 70; //秒   需大于空闲时间

        // 最大协议包长度
        int MAX_FRAME_LENGTH = 1024 * 10; // 10k
        //
        int MAX_AGGREGATED_CONTENT_LENGTH = 65536;

        String SOCKET = "0";//socket标识

        String WEBSOCKET = "1";//websocket标识

        String UDPSOCKET = "2";//udpsocket标识

        int DATA_HEADER_LENGTH = 3; //消息最短长度

        String CLIENT_CONNECT_CLOSED = "client_closed";

        String CLIENT_CONNECT_BIND = "client_bind";
    }

    public interface SessionConfig {
        String SESSION_KEY = "account";
        AttributeKey<String> SERVER_SESSION_ID = AttributeKey.valueOf(SESSION_KEY);
        AttributeKey SERVER_SESSION_HEARBEAT = AttributeKey.valueOf("heartbeat");
        AttributeKey<Integer> PING_COUNT = AttributeKey.valueOf("ping_count");
        AttributeKey<String> UID = AttributeKey.valueOf("uid");
        AttributeKey<String> CHANNEL = AttributeKey.valueOf("channel");
        AttributeKey<String> ID = AttributeKey.valueOf("id");
        AttributeKey<String> DEVICE_ID = AttributeKey.valueOf("device_id");
        AttributeKey<String> GROUP_ID = AttributeKey.valueOf("group_id");
        AttributeKey<String> TAG = AttributeKey.valueOf("tag");
        AttributeKey<String> LANGUAGE = AttributeKey.valueOf("language");
        AttributeKey<String> IPS = AttributeKey.valueOf("ips");
        AttributeKey<Integer> PORT = AttributeKey.valueOf("port");
    }

    public interface ProtobufType {
        byte SEND = 1; //请求
        byte RECEIVE = 2; //接收
        byte NOTIFY = 3; //通知
        byte REPLY = 4; //回复
    }

    public interface CmdType {
        byte PONG = 0;
        byte PING = 1;
        byte MESSAGE = 2;
        byte ONLINE = 3; //上线
        byte OFFLINE = 4; //下线
        byte RECON = 5; //重连
    }

}
