package com.auth2server.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author zl
 * @version 1.0
 * @date 2021/11/2 17:06
 */
@ApiModel
public class RequestEntitys {
    @Data
    @ApiModel(value="登录")
    public static class Login{
        @NotBlank(message = "邮箱不能为空")
        @ApiModelProperty("邮箱")
        private String username;
        @NotBlank(message = "密码不能为空")
        @ApiModelProperty("用户密码")
        private String password;
        @NotBlank(message = "验证码标识不能为空")
        @ApiModelProperty("验证码标识")
        private String uuid;
        @NotBlank(message = "验证码不能为空")
        @ApiModelProperty("验证码")
        private String code;
    }
}
