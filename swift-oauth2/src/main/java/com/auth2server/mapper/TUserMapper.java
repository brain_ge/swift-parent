package com.auth2server.mapper;


import com.auth2server.entity.TUserEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author zl
 * @version 1.0
 * @date 2021/10/26 15:37
 */
public interface TUserMapper extends BaseMapper<TUserEntity> {
}
