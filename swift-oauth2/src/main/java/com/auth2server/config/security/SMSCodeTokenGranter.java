package com.auth2server.config.security;

import com.auth2server.utils.GuavaCacheUtil;
import lombok.SneakyThrows;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.common.exceptions.InvalidGrantException;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.security.oauth2.provider.*;
import org.springframework.security.oauth2.provider.token.AbstractTokenGranter;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;

import javax.naming.AuthenticationException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class SMSCodeTokenGranter extends AbstractTokenGranter {

    private static final String GRANT_TYPE = "sms_code";

    private UserDetailsService userDetailsService;

    public SMSCodeTokenGranter(AuthorizationServerTokenServices tokenServices,
                               ClientDetailsService clientDetailsService,
                               OAuth2RequestFactory requestFactory,
                               UserDetailsService userDetailsService) {
        super(tokenServices, clientDetailsService, requestFactory, GRANT_TYPE);
        this.userDetailsService = userDetailsService;
    }
    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        // 设置默认的加密方式
        return new BCryptPasswordEncoder();
    }

    @SneakyThrows
    @Override
    protected OAuth2Authentication getOAuth2Authentication(ClientDetails client,
                                                           TokenRequest tokenRequest) {

        Map<String, String> parameters = new LinkedHashMap<String, String>(tokenRequest.getRequestParameters());
        String userMobileNo = parameters.get("username");  //客户端提交的用户名
        String smscode = parameters.get("code");  //客户端提交的验证码
        List<GrantedAuthority> authorities = new ArrayList<>();
        // 从库里查用户
        UserDetails user = userDetailsService.loadUserByUsername(userMobileNo);
        if(ObjectUtils.isEmpty(user)) {
            throw new OAuth2Exception("用户不存在");
        }
        //验证用户状态(是否禁用等),代码略
        // 获取服务中保存的用户验证码,代码略.一般我们是在生成好后放到缓存中
        String smsCodeCached = GuavaCacheUtil.getIfPresent(parameters.get("uuid"))!=null ?
                GuavaCacheUtil.getIfPresent(parameters.get("uuid")).toString() : null;
        if(StringUtils.isBlank(smsCodeCached)) {
            throw new OAuth2Exception("用户没有发送验证码");
        }
        if(!smscode.equals(smsCodeCached)) {
            throw new OAuth2Exception("验证码不正确");
        }else {
        //验证通过后从缓存中移除验证码,代码略
        }


        Authentication userAuth = new UsernamePasswordAuthenticationToken(user, authorities, user.getAuthorities());

        // 关于user.getAuthorities(): 我们的自定义用户实体是实现了
        // org.springframework.security.core.userdetails.UserDetails 接口的, 所以有 user.getAuthorities()
        // 当然该参数传null也行
        ((AbstractAuthenticationToken) userAuth).setDetails(parameters);

        OAuth2Request storedOAuth2Request = getRequestFactory().createOAuth2Request(client, tokenRequest);
        return new OAuth2Authentication(storedOAuth2Request, userAuth);
    }

}
