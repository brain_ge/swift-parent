package com.auth2server.service;

import com.auth2server.entity.TUserEntity;
import com.auth2server.mapper.TUserMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @author zl
 * @version 1.0
 * @date 2021/10/26 15:35
 */
@Service
public class TUserService extends ServiceImpl<TUserMapper, TUserEntity> {
}
