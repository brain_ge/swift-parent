package com.auth2server.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
/**
 * @author zl
 * @version 1.0
 * @date 2021/10/26 15:32
 */
@Data
@TableName("t_user")
public class TUserEntity {

    @TableId(type = IdType.AUTO)
    private Integer id;

    private String email;

    private String nickname;

    private String name;

    private String password;

    private String phone;

    private Integer age;

    private Integer sex;

    private String createTime ;
    private String updateTime;

    private Integer logic = 0;
}
