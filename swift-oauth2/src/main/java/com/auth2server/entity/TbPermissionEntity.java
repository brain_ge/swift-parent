package com.auth2server.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 权限表
 * 
 * @author zl
 * @email lin5178@126.com
 * @date 2020-09-08 17:16:35
 */
@Data
public class TbPermissionEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * $column.comments
	 */
	private Long id;
	/**
	 * 父权限
	 */
	private Long parentId;
	/**
	 * 权限名称
	 */
	private String name;
	/**
	 * 权限英文名称
	 */
	private String enname;
	/**
	 * 授权路径
	 */
	private String url;
	/**
	 * 备注
	 */
	private String description;
	/**
	 * $column.comments
	 */
	private Date created;
	/**
	 * $column.comments
	 */
	private Date updated;

}
