package com.netty.web.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.netty.utils.DateUtils;
import com.netty.utils.GuavaCacheUtil;
import com.netty.utils.R;
import com.netty.web.entity.TUserEntity;
import com.netty.web.service.TUserService;
import com.netty.web.vo.RequestEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Date;
import java.util.Objects;

/**
 * @author zl
 * @version 1.0
 * @date 2021/10/26 15:25
 */
@Api(tags = "用户操作")
@RestController
@RequestMapping("/user")
public class TUserController {

    @Autowired
    private TUserService tUserService;

    @ApiOperation("创建用户")
    @PostMapping("/create")
    public R createUser(@Validated @RequestBody TUserEntity tUserEntity)  {
        try{
            if(Arrays.asList(0,1).indexOf(tUserEntity.getSex())==-1) throw new Exception("性别 0 男 1 女，其他数值不支持");
            QueryWrapper<TUserEntity> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("email",tUserEntity.getEmail());
            TUserEntity one = tUserService.getOne(queryWrapper);
            if(!ObjectUtils.isEmpty(one)) throw new Exception("用户已存在！");
            tUserEntity.setCreateTime(DateUtils.getDateTime());
            boolean save = tUserService.save(tUserEntity);
            if(save) return R.ok("创建成功！");
            throw new Exception("创建失败，请联系工作人员！");
        }catch (Exception e){
            return R.error(e.getMessage());
        }
    }

    @ApiOperation("注销用户")
    @PostMapping("/delete")
    public R deleteUser(@Validated @RequestBody RequestEntity.Q2dUser q2dUser) {
        try{
            R r = queryUser(q2dUser.getEmail(), q2dUser.getPassword());
            if(!Objects.equals(r.get("code"),0)) throw new Exception(r.get("msg")+"");
            TUserEntity one = (TUserEntity) r.get("data");
            TUserEntity tUserEntity = new TUserEntity();
            tUserEntity.setId(one.getId());
            tUserEntity.setLogic(-1);
            tUserEntity.setUpdateTime(DateUtils.getDateTime());
            boolean b = tUserService.updateById(tUserEntity);
            if(b){
                return R.ok("注销成功!");
            }
            throw new Exception("注销失败，请联系工作人员！");
        }catch (Exception e){
            return R.error(e.getMessage());
        }
    }

    @ApiOperation("修改用户")
    @PostMapping("/update")
    public R updateUser(@Validated @RequestBody TUserEntity tUserEntity) {
        try{
            QueryWrapper<TUserEntity> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("email",tUserEntity.getEmail()).eq("logic",0);
            TUserEntity one = tUserService.getOne(queryWrapper);
            tUserEntity.setId(one.getId());
            tUserEntity.setUpdateTime(DateUtils.getDateTime());
            boolean b = tUserService.updateById(tUserEntity);
            if(b) return R.ok("修改成功!");
            throw new Exception("修改失败，请联系工作人员!");
        }catch (Exception e){
            return R.error(e.getMessage());
        }
    }


    @ApiOperation("查询用户信息")
    @GetMapping("/query")
    public R queryUser(@RequestParam("email") String email,
                       @RequestParam("password") String password){
        try{
            QueryWrapper<TUserEntity> userEntityQueryWrapper = new QueryWrapper<>();
            userEntityQueryWrapper.eq("email",email).eq("logic",0);
            TUserEntity one = tUserService.getOne(userEntityQueryWrapper);
            if(ObjectUtils.isEmpty(one)) throw new Exception("用户名不存在!");
            //判断用户密码是否相等
            if(Objects.equals(password,one.getPassword())) return R.ok().put("data",one);
            throw new Exception("密码错误");
        }catch (Exception e){
            return R.error(e.getMessage());
        }
    }

    @ApiOperation("修改密码")
    @PostMapping("/updatePassword")
    public R updatePassword(@Validated @RequestBody RequestEntity.EditUser editUser){
        try{
            TUserEntity tUserEntity = new TUserEntity();
            tUserEntity.setEmail(editUser.getEmail());
            tUserEntity.setPassword(editUser.getPassword());
            R r = updateUser(tUserEntity);
            return r;
        }catch (Exception e){
            return R.error(e.getMessage());
        }
    }


}
