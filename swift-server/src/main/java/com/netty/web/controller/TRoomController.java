package com.netty.web.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.netty.utils.DateUtils;
import com.netty.utils.R;
import com.netty.web.entity.TRoomEntity;
import com.netty.web.entity.TUserEntity;
import com.netty.web.service.TRoomService;
import com.netty.web.vo.RequestEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.UUID;

/**
 * @author zl
 * @version 1.0
 * @date 2021/10/26 15:25
 */
@Api(tags = "房间操作")
@RestController
@RequestMapping("/room")
public class TRoomController {
    @Autowired
    private TRoomService tRoomService;

    @ApiOperation("创建房间")
    @PostMapping("/create")
    public R createRoom(@Validated  @RequestBody TRoomEntity tRoomEntity){
        try{
            QueryWrapper<TRoomEntity> tRoomEntityQueryWrapper = new QueryWrapper<>();
            tRoomEntityQueryWrapper.eq("room_code",tRoomEntity.getRoomCode())
                    .or().eq("name",tRoomEntity.getName());
            TRoomEntity one = tRoomService.getOne(tRoomEntityQueryWrapper);
            if(!ObjectUtils.isEmpty(one)) throw new Exception("当前房间编码或房间名称已存在，请重新编写！");
            tRoomEntity.setCreateTime(DateUtils.getDateTime());
            boolean save = tRoomService.save(tRoomEntity);
            if(save) return R.ok("创建成功！");
            throw new Exception("创建失败，请联系工作人员！");
        }catch (Exception e){
            return R.error(e.getMessage());
        }
    }

    @ApiOperation("删除房间")
    @PostMapping("/delete")
    public R deleteRoom(@Validated  @RequestBody RequestEntity.DeleteRoom deleteRoom){
        try{
            QueryWrapper<TRoomEntity> tRoomEntityQueryWrapper = new QueryWrapper<>();
            tRoomEntityQueryWrapper.eq("room_code",deleteRoom.getRoomCode())
                    .eq("user_id",deleteRoom.getUserId())
                    .eq("logic","0");
            TRoomEntity one = tRoomService.getOne(tRoomEntityQueryWrapper);
            if(ObjectUtils.isEmpty(one)) throw new Exception("没有该房间信息");
            one.setLogic(-1);
            one.setOldName(one.getName());
            one.setOldCode(one.getRoomCode());
            one.setRoomCode(UUID.randomUUID().toString());
            one.setName(UUID.randomUUID().toString());
            boolean b = tRoomService.updateById(one);
            if(b) return R.ok("删除成功");
            throw new Exception("删除失败，请联系工作人员！");
        }catch (Exception e){
            return R.error(e.getMessage());
        }
    }

    @ApiOperation("查询房间")
    @GetMapping("/query")
    public R queryRoom(@ApiParam(value = "用户id",required = true) @RequestParam("userId") Integer userId,
                       @ApiParam(value = "页数",required = true) @RequestParam("page") Integer page,
                       @ApiParam(value = "每页的条数",required = true) @RequestParam("pageSize") Integer pageSize){
        QueryWrapper<TRoomEntity> tRoomEntityQueryWrapper = new QueryWrapper<>();
        tRoomEntityQueryWrapper.eq("user_id",userId)
                .eq("logic","0");
        Page<TRoomEntity> lists = tRoomService.page(new Page<>(page, pageSize), tRoomEntityQueryWrapper);
        return R.ok().put("data",page);
    }






}
