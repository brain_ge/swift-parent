package com.netty.web.controller;

import com.alibaba.fastjson.JSON;
import com.netty.component.push.impl.DefaultMessagePusher;
import com.netty.constant.ImChannelGroup;
import com.netty.model.Message;
import com.netty.model.ReplyBody;
import com.netty.utils.R;
import com.netty.web.convert.MessageCovertBasic;
import com.netty.web.utils.ImageUtils;
import com.netty.web.vo.MessageEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;

@Api(tags = "消息推送")
@Slf4j
@RestController
@RequestMapping("/")
public class TSessionController {
    @Autowired
    private DefaultMessagePusher defaultMessagePusher;
    @Value("${upload.filePath}")
    private  String filePath;

    @ApiOperation(value = "单节点调用该方法")
    @PostMapping(value = "/send")
    public R send(@Validated @RequestBody MessageEntity messageEntity) {
        try {
            Message message = MessageCovertBasic.INSTANCE.toCovertMessage(messageEntity);
            if(Objects.equals(message.getAction(),"gif")){
                String content = message.getContent();
                String fileName = UUID.randomUUID().toString()+".png";
                ImageUtils.base64StringToImage(content.split(",")[1],filePath+fileName);
                message.setContent("/upload/"+fileName);
            }
            defaultMessagePusher.push(message);
            return R.ok();
        } catch (Exception e) {
            e.printStackTrace();
            return R.error(e.getMessage());
        }
    }

    @ApiOperation(value = "群发调用该方法")
    @PostMapping(value = "/sendAll")
    public R sendAll(@Validated @RequestBody MessageEntity messageEntity) {
        try {
            Message message = MessageCovertBasic.INSTANCE.toCovertMessage(messageEntity);
            if(Objects.equals(message.getAction(),"gif")){
                String content = message.getContent();
                String fileName = UUID.randomUUID().toString()+".png";
                ImageUtils.base64StringToImage(content.split(",")[1],filePath+fileName);
                message.setContent("/upload/"+fileName);
            }
            ReplyBody replyBody = new ReplyBody();
            replyBody.setKey("message");
            replyBody.setTimestamp(new Date().getTime());
            replyBody.put("body", JSON.toJSONString(message));
            defaultMessagePusher.pushAll(replyBody);
            return R.ok();
        } catch (Exception e) {
            e.printStackTrace();
            return R.error(e.getMessage());
        }
    }

    @ApiOperation(value = "断开所有链接")
    @PostMapping("disconnect")
    public R disconnect() {
        ImChannelGroup.disconnect();
        return R.ok();
    }


}
