package com.netty.web.config;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author zl
 * @version 1.0
 * @date 2021/10/29 14:59
 */
@Configuration
public class MyBatisPlusPageConfig  {
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }
}
