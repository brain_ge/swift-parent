package com.netty.web.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.netty.web.entity.TRoomEntity;

/**
 * @author zl
 * @version 1.0
 * @date 2021/10/27 14:04
 */
public interface TRoomMapper  extends BaseMapper<TRoomEntity> {
}
