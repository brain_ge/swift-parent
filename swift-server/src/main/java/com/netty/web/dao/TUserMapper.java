package com.netty.web.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.netty.web.entity.TUserEntity;

/**
 * @author zl
 * @version 1.0
 * @date 2021/10/26 15:37
 */
public interface TUserMapper extends BaseMapper<TUserEntity> {
}
