package com.netty.web.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.netty.web.dao.TRoomMapper;
import com.netty.web.entity.TRoomEntity;
import org.springframework.stereotype.Service;

/**
 * @author zl
 * @version 1.0
 * @date 2021/10/27 14:04
 */
@Service
public class TRoomService extends ServiceImpl<TRoomMapper, TRoomEntity> {
}
