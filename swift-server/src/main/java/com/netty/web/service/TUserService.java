package com.netty.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.netty.web.dao.TUserMapper;
import com.netty.web.entity.TUserEntity;
import org.springframework.stereotype.Service;

/**
 * @author zl
 * @version 1.0
 * @date 2021/10/26 15:35
 */
@Service
public class TUserService extends ServiceImpl<TUserMapper, TUserEntity>  {
}
