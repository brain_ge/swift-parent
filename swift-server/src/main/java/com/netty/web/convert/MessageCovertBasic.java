package com.netty.web.convert;

import com.netty.model.Message;
import com.netty.web.vo.MessageEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

import java.util.Date;
import java.util.Objects;

/**
 * @author zl
 * @version 1.0
 * @date 2021/9/13 17:36
 */
@Mapper
public interface MessageCovertBasic {

    MessageCovertBasic INSTANCE = Mappers.getMapper(MessageCovertBasic.class);

    @Mappings({
            @Mapping(source = "timestamp", target = "timestamp", qualifiedByName = "getTimestamp"),
            @Mapping(source = "action", target = "action", qualifiedByName = "updateAction"),
    })
    Message toCovertMessage(MessageEntity messageEntity);

    @Named("getTimestamp")
    default Long getTimestamp(Long timestamp) {
        return new Date().getTime();
    }

    @Named("updateAction")
    default String updateAction(String action) {
        if (Objects.isNull(action)) {
            return "string";
        }
        return action;
    }
}
