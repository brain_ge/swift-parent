package com.netty.web.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author zl
 * @version 1.0
 * @date 2021/10/26 15:32
 */
@Data
@TableName("t_user")
@ApiModel
public class TUserEntity {

    @TableId(type = IdType.AUTO)
    @ApiModelProperty("唯一编码")
    private Integer id;

    @NotBlank(message = "邮箱不能为空")
    @ApiModelProperty("邮箱")
    private String email;

    @ApiModelProperty("昵称")
    private String nickname;

    @ApiModelProperty("名称")
    private String name;

    @NotBlank(message = "登陆密码")
    @ApiModelProperty("登陆密码")
    @JsonIgnore
    private String password;

    @ApiModelProperty("手机号")
    private String phone;

    @ApiModelProperty("年龄")
    private Integer age;

    @ApiModelProperty("性别 0 男 1 女")
    private Integer sex;

    @ApiModelProperty("创建时间")
    @JsonIgnore
    private String createTime ;
    @JsonIgnore
    @ApiModelProperty("更新时间")
    private String updateTime;

    @JsonIgnore
    @ApiModelProperty("是否逻辑删除 0 正常 -1 删除")
    private Integer logic = 0;
}
