package com.netty.web.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.netty.utils.DateUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author zl
 * @version 1.0
 * @date 2021/10/27 13:55
 */
@Data
@TableName("t_room")
@ApiModel
public class TRoomEntity {

    @TableId(type = IdType.AUTO)
    @ApiModelProperty("唯一编码")
    @JsonIgnore
    private Integer id;

    @ApiModelProperty("房间编码")
    @NotBlank(message = "房间编码不能为空")
    private String roomCode;

    @ApiModelProperty("删除之后存储原房间编码")
    private String oldCode;

    @ApiModelProperty("房间名称")
    @NotBlank(message = "房间名称不能为空")
    private String name;

    @ApiModelProperty("删除之后存储原房间名称")
    private String oldName;

    @ApiModelProperty("房间密码")
    private String password;

    @ApiModelProperty("用户id")
    @NotNull(message = "用户id不能为空")
    private Integer userId;

    @ApiModelProperty("创建时间")
    @JsonIgnore
    private String createTime;

    @ApiModelProperty("是否开放 true 开放 false 私密 私密时password 不能为空")
    private Boolean open = true;

    @ApiModelProperty("排序")
    private Integer sort;

    @ApiModelProperty("是否删除")
    @JsonIgnore
    private Integer logic = 0;
}
