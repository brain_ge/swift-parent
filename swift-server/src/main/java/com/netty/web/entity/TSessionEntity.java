package com.netty.web.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 存放登录信息
 */
@Data
@TableName("t_session")
public class TSessionEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    public static final transient int STATE_ACTIVE = 0;
    public static final transient int STATE_APNS = 1;
    public static final transient int STATE_INACTIVE = 2;

    public static final transient String CHANNEL_IOS = "ios";
    public static final transient String CHANNEL_ANDROID = "android";
    public static final transient String CHANNEL_WINDOWS = "windows";
    public static final transient String CHANNEL_MAC = "mac";
    public static final transient String CHANNEL_WEB = "web";

    /**
     * 数据库主键ID
     */
    @TableId(type = IdType.AUTO)
    private Long id;
    /**
     * 终端应用版本
     */
    private String appVersion;
    /**
     * 登录时间
     */
    private Long bindTime;
    /**
     * 终端设备类型
     */
    private String channel;
    /**
     * 客户端ID (设备号码+应用包名),ios为deviceToken
     */
    private String deviceId;
    /**
     * 终端设备型号
     */
    private String deviceName;
    /**
     * session绑定的服务器IP
     */
    private String host;
    /**
     * 终端语言
     */
    private String language;
    /**
     * 维度
     */
    private Double latitude;
    /**
     * 位置
     */
    private String location;
    /**
     * 经度
     */
    private Double longitude;
    /**
     * session在本台服务器上的ID
     */
    private String nid;
    /**
     * 终端系统版本
     */
    private String osVersion;
    /**
     * 状态
     */
    private Integer state;
    /**
     * session绑定的用户账号
     */
    private String uid;

}
