package com.netty.web.timer;

import com.alibaba.fastjson.JSON;
import com.netty.constant.Constants;
import com.netty.constant.SessionGroup;
import com.netty.model.Message;
import com.netty.model.ReplyBody;
import com.netty.model.proto.SentBodyProto;
import com.netty.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author zl
 * @version 1.0
 * @date 2021/10/15 16:59
 */
@Component
public class TimerTask {

    @Autowired
    private SessionGroup sessionGroup;

    @Scheduled(fixedDelay=60*1000)
    public void send2User(){
        ReplyBody replys = new ReplyBody();
        replys.setKey(Constants.ImserverConfig.CLIENT_CONNECT_BIND);
        replys.put("body", JSON.toJSONString(sessionGroup.findAll()));
        sessionGroup.writep2All(replys);
    }


}
