package com.netty.web.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author zl
 * @version 1.0
 * @date 2021/9/13 17:35
 */
@Data
@ApiModel
public class MessageEntity {

    /**
     * 消息类型，用户自定义消息类别
     */
    @NotNull(message = "action 不能为空")
    @ApiModelProperty("消息类型，用户自定义消息类别")
    private String action;
    /**
     * 消息标题
     */
    @ApiModelProperty("消息标题")
    private String title;
    /**
     * 消息类容，于action 组合为任何类型消息，content 根据 format 可表示为 text,json ,xml数据格式
     */
    @NotNull(message = "content 不能为空")
    @ApiModelProperty("content 不能为空")
    private String content;

    /**
     * 消息发送者账号
     */
    @NotNull(message = "sender 不能为空")
    @ApiModelProperty("消息发送者账号")
    private String sender;
    /**
     * 消息发送者接收者
     */
    @NotNull(message = "receiver 不能为空")
    @ApiModelProperty("消息发送者接收者")
    private String receiver;
    /**
     * group 为一编码
     */
    @ApiModelProperty("group id")
    private String groupId;

    @JsonIgnore
    private long timestamp;
}
