package com.netty.web.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.Data;
import lombok.NonNull;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author zl
 * @version 1.0
 * @date 2021/10/27 10:40
 */
@ApiModel("用户操作实体类")
public class RequestEntity {
    @Data
    @ApiModel(value="登录")
    public static class login{
        @NotBlank(message = "邮箱不能为空")
        @ApiModelProperty("邮箱")
        private String email;
        @NotBlank(message = "密码不能为空")
        @ApiModelProperty("用户密码")
        private String password;
        @NotBlank(message = "验证码标识不能为空")
        @ApiModelProperty("验证码标识")
        private String uuid;
        @NotBlank(message = "验证码不能为空")
        @ApiModelProperty("验证码")
        private String code;
    }

    @ApiModel(value="修改用户")
    @Data
    public static class EditUser{
        @NotBlank(message = "邮箱不能为空")
        @ApiModelProperty("邮箱")
        private String email;
        @NotBlank(message = "密码不能为空")
        @ApiModelProperty("用户密码")
        private String password;
        @NotBlank(message = "验证码不能为空")
        @ApiModelProperty("验证码")
        private String code;
    }

    @ApiModel(value="删除或查询用户")
    @Data
    public static class Q2dUser{
        @NotBlank(message = "邮箱不能为空")
        @ApiModelProperty("邮箱")
        private String email;
        @NotBlank(message = "密码不能为空")
        @ApiModelProperty("用户密码")
        private String password;
    }


    @ApiModel(value="删除房间")
    @Data
    public static class DeleteRoom{
        @NotBlank(message = "房间编码不能为空")
        @ApiModelProperty("房间编码")
        private String roomCode;
        @NotNull(message = "用户id不能为空")
        @ApiModelProperty("用户id")
        private Integer userId;
    }
    @ApiModel(value="发送邮件")
    @Data
    public static class SendEmail{
        @NotBlank(message = "邮箱地址不能为空")
        @ApiModelProperty("邮箱地址")
        private String email;
        @NotBlank(message = "邮件主题不能为空")
        @ApiModelProperty("邮件主题")
        private String subject;
    }
}
