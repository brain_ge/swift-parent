package com.netty;

import com.netty.constant.SessionGroup;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@MapperScan("com.netty.web.dao")
@EnableScheduling
public class SwiftNettyCodeApplication {

    public static void main(String[] args) {
        SpringApplication.run(SwiftNettyCodeApplication.class, args);
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    public SessionGroup sessionGroup() {
        return new SessionGroup();
    }
}
