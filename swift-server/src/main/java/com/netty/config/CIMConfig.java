package com.netty.config;

import com.netty.*;
import com.netty.config.properties.CIMProperties;
import com.netty.constant.SessionGroup;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

/**
 * @author zl
 * @version 1.0
 * 项目启动成功之后在启动netty
 * @date 2021/9/17 17:56
 */
@Slf4j
@Configuration
public class CIMConfig implements ApplicationListener<ApplicationStartedEvent> {
    @Resource
    private ApplicationContext applicationContext;
    @Autowired
    private SessionGroup sessionGroup;
    @Override
    public void onApplicationEvent(ApplicationStartedEvent applicationStartedEvent) {
        try {
            ImWebsocketServer imWebsocketServer = new ImWebsocketServer(new CIMWebSocketConfig(sessionGroup), 34567);
            imWebsocketServer.startServer();
//            ImSocketServer imSocketServer = new ImSocketServer(new CIMSocketConfig(sessionGroup), 23456);
//            imSocketServer.startServer();
//            ImUdpServer imUdpServer = new ImUdpServer(new CIMUdpConfig(), 45678);
//            imUdpServer.startServer();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
