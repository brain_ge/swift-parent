package com.netty.config;

import com.netty.CIMSocketRequestHandler;
import com.netty.ImSocketServer;
import com.netty.config.properties.CIMProperties;
import com.netty.constant.Constants;
import com.netty.constant.ImChannelGroup;
import com.netty.constant.SessionGroup;
import com.netty.model.proto.ReplyBodyProto;
import com.netty.model.proto.SentBodyProto;
import io.netty.channel.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @author zl
 * @version 1.0
 * @date 2021/9/17 17:43
 */
@Slf4j
public class CIMSocketConfig implements CIMSocketRequestHandler {
    @Autowired
    private SessionGroup sessionGroup;

    private String host;

    @Value("${swift.socket.port}")
    private Integer port;

    public CIMSocketConfig(SessionGroup sessionGroup) throws UnknownHostException {
        host = InetAddress.getLocalHost().getHostAddress();
        this.sessionGroup = sessionGroup;
    }

    @Override
    public void channelActive(Channel channel) {
        System.out.println("链接成功");
    }

    @Override
    public void channelRead0(Channel channel, Object body) {
        if (body instanceof SentBodyProto.Model) {
            SentBodyProto.Model message = (SentBodyProto.Model) body;
            switch (message.getKey()){
                case Constants.ImserverConfig.CLIENT_CONNECT_BIND:
                    ReplyBodyProto.Model.Builder reply = ReplyBodyProto.Model.newBuilder();
                    reply.setKey(message.getKey());
                    reply.setCode(HttpStatus.OK.value() + "");
                    reply.setTimestamp(System.currentTimeMillis());
                    String uid = message.getDataOrThrow("uid");
                    channel.attr(Constants.SessionConfig.UID).set(uid);
                    channel.attr(Constants.SessionConfig.CHANNEL).set(Constants.ImserverConfig.SOCKET);
                    channel.attr(Constants.SessionConfig.DEVICE_ID).set(message.getDataOrThrow("deviceId"));
                    channel.attr(Constants.SessionConfig.IPS).set(host);
                    channel.attr(Constants.SessionConfig.PORT).set(port);
                    /*
                     * 添加到内存管理
                     */
                    sessionGroup.add(channel);
                    /**
                     * 将链接信息添加到组
                     */
                    ImChannelGroup.add(channel);
                    /*
                     *向客户端发送bind响应
                     */
                    channel.writeAndFlush(reply);
                    break;
                case Constants.ImserverConfig.CLIENT_CONNECT_CLOSED:
                    channelUnregistered(channel);
                    break;
            }
        }
        /*
         * 发送上线事件到集群中的其他实例，控制其他设备下线
         */
        //signalRedisTemplate.bind(session);
    }

    @Override
    public void channelUnregistered(Channel channel) {
        sessionGroup.remove(channel);
        /**
         * 将链接信息删除
         */
        ImChannelGroup.remove(channel);
    }
}
