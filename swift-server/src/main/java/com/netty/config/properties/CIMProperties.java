/*
 * Copyright 2013-2019 Xia Jun(3979434@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ***************************************************************************************
 *                                                                                     *
 *                        Website : http://www.farsunset.com                           *
 *                                                                                     *
 ***************************************************************************************
 */
package com.netty.config.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "swift")
public class CIMProperties {

    private final Scoket socket = new Scoket();

    private final Websocket websocket = new Websocket();

    private final Udpsocket udpsocket = new Udpsocket();

    public Scoket getSocket() {
        return socket;
    }

    public Websocket getWebsocket() {
        return websocket;
    }

    public Udpsocket getUdpsocket() {
        return udpsocket;
    }

    public static class Scoket {

        private Integer port;

        public void setPort(Integer port) {
            this.port = port;
        }

        public Integer getPort() {
            return port;
        }
    }

    public static class Websocket {
        private Integer port;

        public Integer getPort() {
            return port;
        }

        public void setPort(Integer port) {
            this.port = port;
        }
    }

    public static class Udpsocket {
        private Integer port;

        public Integer getPort() {
            return port;
        }

        public void setPort(Integer port) {
            this.port = port;
        }
    }

    public Integer getSocketPort() {
        return socket.port;
    }

    public Integer getWebsocketPort() {
        return websocket.port;
    }

    public Integer getUdpsocketPort() {
        return udpsocket.port;
    }

}
