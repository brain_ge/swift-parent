package com.netty.config;

import com.alibaba.fastjson.JSON;
import com.netty.CIMWebSocketRequestHandler;
import com.netty.ImWebsocketServer;
import com.netty.config.properties.CIMProperties;
import com.netty.constant.Constants;
import com.netty.constant.ImChannelGroup;
import com.netty.constant.SessionGroup;
import com.netty.model.ReplyBody;
import com.netty.model.proto.SentBodyProto;
import io.netty.channel.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;

import java.net.InetAddress;
import java.net.UnknownHostException;


@Slf4j
public class CIMWebSocketConfig implements CIMWebSocketRequestHandler {

	private SessionGroup sessionGroup;

	@Value("${server.port}")
	private Integer port;

	private String host;

	public CIMWebSocketConfig(SessionGroup sessionGroup) throws UnknownHostException {
		host = InetAddress.getLocalHost().getHostAddress();
		this.sessionGroup = sessionGroup;
	}

	@Bean
	public ImWebsocketServer getNioWebsocketAcceptor(CIMProperties properties) {
		return new ImWebsocketServer(this,properties.getWebsocketPort());
	}
	@Override
	public void channelActive(Channel channel) {
		System.out.println("链接成功");
	}

	@Override
	public void channelRead0(Channel channel, SentBodyProto.Model sentBody) {
		if (sentBody instanceof SentBodyProto.Model) {
			switch (sentBody.getKey()){
				case Constants.ImserverConfig.CLIENT_CONNECT_BIND:
					ReplyBody reply = new ReplyBody();
					reply.setKey(sentBody.getKey());
					reply.setCode(HttpStatus.OK.value() + "");
					reply.setTimestamp(System.currentTimeMillis());
					String uid = sentBody.getDataOrThrow("uid");
					channel.attr(Constants.SessionConfig.UID).set(uid);
					channel.attr(Constants.SessionConfig.CHANNEL).set(Constants.ImserverConfig.WEBSOCKET);
					channel.attr(Constants.SessionConfig.DEVICE_ID).set(sentBody.getDataOrThrow("deviceId"));
					channel.attr(Constants.SessionConfig.IPS).set(host);
					channel.attr(Constants.SessionConfig.PORT).set(port);
					/*
					 * 添加到内存管理
					 */
					sessionGroup.add(channel);
					/**
					 * 将链接信息添加到组
					 */
					ImChannelGroup.add(channel);
					/*
					 *向客户端发送bind响应
					 */
					//channel.writeAndFlush(reply);
					ReplyBody replys = new ReplyBody();
					replys.setKey(Constants.ImserverConfig.CLIENT_CONNECT_BIND);
					replys.put("body", JSON.toJSONString(sessionGroup.findAll()));
					sessionGroup.writep2All(replys);
					break;
				case Constants.ImserverConfig.CLIENT_CONNECT_CLOSED:
					channelUnregistered(channel);
					break;
			}
		}
	}

	@Override
	public void channelUnregistered(Channel channel) {
		sessionGroup.remove(channel);
		/**
		 * 将链接信息删除
		 */
		ImChannelGroup.remove(channel);
		ReplyBody replys = new ReplyBody();
		replys.setKey(Constants.ImserverConfig.CLIENT_CONNECT_BIND);
		replys.put("body", JSON.toJSONString(sessionGroup.findAll()));
		sessionGroup.writep2All(replys);
	}
}