package com.netty.email.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author zl
 * @version 1.0
 * @date 2021/10/26 9:14
 */
@Data
@ApiModel
public class EmailEntity {
    /**
     * 主题
     */
    @NotNull(message = "发送主题不能为空")
    @ApiModelProperty("主题")
    String subject;
    /**
     * 内容
     */
    @ApiModelProperty("内容")
    String content;
    /**
     * 需要发送的人
     */
    @NotNull(message = "需要发送的人不能为空")
    @ApiModelProperty("需要发送的人")
    String[] toWho;
    /**
     * 需要抄送的人
     */
    @ApiModelProperty("需要抄送的人")
    String[] ccPeoples;
    /**
     * 需要密送的人
     */
    @ApiModelProperty("需要密送的人")
    String[] bccPeoples;
    /**
     * 需要附带的附件
     */
    @ApiModelProperty("需要附带的附件")
    String[] attachments;
}
