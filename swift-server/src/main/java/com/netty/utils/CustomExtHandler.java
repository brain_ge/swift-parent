package com.netty.utils;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

/**
 * @author zl
 * @version 1.0
 * @date 2021/9/17 14:57
 */
//如果返回的是Json格式的数据，
//可以使用@ResponseBody+@ControllerAdvice替换@RestControllerAdvice
@RestControllerAdvice
public class CustomExtHandler {
    private static final Logger LOG = LoggerFactory.getLogger(CustomExtHandler.class);

    //捕获全局异常，处理所有不可知的异常
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public R handleException(MethodArgumentNotValidException e, HttpServletRequest request) {
        LOG.info("url:{},msg:{}", request.getRequestURL(), e.getMessage());
        return R.error(500, e.getBindingResult().getFieldError().getDefaultMessage());
    }

}
