/*
 * Copyright 2013-2019 Xia Jun(3979434@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ***************************************************************************************
 *                                                                                     *
 *                        Website : http://www.farsunset.com                           *
 *                                                                                     *
 ***************************************************************************************
 */
package com.netty.component.push.impl;

import com.alibaba.fastjson.JSON;
import com.netty.component.push.CIMMessagePusher;
import com.netty.constant.SessionGroup;
import com.netty.model.Message;
import com.netty.model.ReplyBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

/*
 * 消息发送实现类
 *
 */
@Component
public class DefaultMessagePusher implements CIMMessagePusher {
    @Autowired
    private SessionGroup sessionGroup;

    /**
     * 向用户发送消息
     *
     * @param message
     */
    public final void push(Message message) {
        String uid = message.getReceiver();
        ReplyBody replyBody = new ReplyBody();
        replyBody.setTimestamp(new Date().getTime());
        replyBody.setKey("message");
        replyBody.put("body", JSON.toJSONString(message));
        sessionGroup.writep2p(uid, replyBody);
    }

    /**
     * 向用户发送消息
     *
     * @param replyBody
     */
    public final void pushAll(ReplyBody replyBody) {
        sessionGroup.writep2All(replyBody);
    }
    /**
     * 向用户发送消息
     *
     * @param replyBody
     */
    public final void push2Group(String group_id,ReplyBody replyBody) {
        sessionGroup.writep2Group(group_id,replyBody);
    }
}
